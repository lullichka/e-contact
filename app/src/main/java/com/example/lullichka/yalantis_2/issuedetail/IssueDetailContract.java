package com.example.lullichka.yalantis_2.issuedetail;


import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by lullichka on 18.05.16.
 */

public interface IssueDetailContract {

    interface View {

        void showCreatedDate(String createdDate);

        void showRegisterDate(String registerDate);

        void showDeadline(String deadline);

        void showDepartment(String department);

        void showDescription(String description);

        void showState(String state);

        void showPictures(List<String> picturesUrls);

    }

    interface UserActionsListener {

        void openIssue (@Nullable int issueId);
    }
}
