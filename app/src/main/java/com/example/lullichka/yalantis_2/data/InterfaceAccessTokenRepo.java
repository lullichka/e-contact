package com.example.lullichka.yalantis_2.data;

import com.facebook.AccessToken;
import com.facebook.Profile;

/**
 * Created by lullichka on 28.05.16.
 * Interface for AccessToken Repository
 */

public interface InterfaceAccessTokenRepo {

    interface SaveTokenCallback {

        void onTokenSaved(String localUserId);
    }


    void saveToken(Profile profile, AccessToken accessToken, SaveTokenCallback callback);

}
