package com.example.lullichka.yalantis_2.login;

import android.util.Log;

import com.example.lullichka.yalantis_2.data.InterfaceAccessTokenRepo;
import com.facebook.AccessToken;
import com.facebook.Profile;

/**
 * Created by lullichka on 28.05.16.
 */
public class LoginActivityPresenter implements LoginActivityContract.UserActionsListener {

    private static final String LOG_TAG = "mylog";
    private final InterfaceAccessTokenRepo mInterfaceAccessTokenRepo;
    private final LoginActivityContract.View mLoginActivityView;

    public LoginActivityPresenter(InterfaceAccessTokenRepo interfaceAccessTokenRepo,
                                  LoginActivityContract.View loginActivityView){
        mInterfaceAccessTokenRepo = interfaceAccessTokenRepo;
        mLoginActivityView = loginActivityView;
    }

    @Override
    public void saveToken(Profile profile, AccessToken accessToken) {
        mInterfaceAccessTokenRepo.saveToken(profile, accessToken, new InterfaceAccessTokenRepo.SaveTokenCallback() {
            @Override
            public void onTokenSaved(String localUserId) {
                Log.d(LOG_TAG, "token saved");
            }
        });
    }

}

