package com.example.lullichka.yalantis_2.data;

import com.example.lullichka.yalantis_2.model.Issue;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by lullichka on 12.05.16.
 * Interface for API server work with/
 */

public interface APIService {
    @GET("tickets")
    Call<List<Issue>> getIssues(@Query("offset") int offset, @Query("amount")int amount);
}
