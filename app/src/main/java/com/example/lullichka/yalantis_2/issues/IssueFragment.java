package com.example.lullichka.yalantis_2.issues;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lullichka.yalantis_2.R;
import com.example.lullichka.yalantis_2.adapters.IssuesAdapter;
import com.example.lullichka.yalantis_2.data.IssueRepository;
import com.example.lullichka.yalantis_2.issuedetail.IssueDetailActivity;
import com.example.lullichka.yalantis_2.model.Issue;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment which uses RecyclerViewAdapter,
 * needs argument STATUS of Issues for opening (pending Issues, done Issues ot Issues at work)
 */


public class IssueFragment extends Fragment implements IssuesContract.View {
    private static final String FRAGMENT_STATUS_ARG = "status";
    private static final String LOG_TAG = "mylog";

    private LinearLayoutManager mLinearLayoutManager;
    private IssuesPresenter mActionsListener;
    private IssuesAdapter mIssuesAdapter;
    private int mState;
    boolean mLoading = true;

    public static IssueFragment newInstance(int sectionNumber) {
        IssueFragment fragment = new IssueFragment();
        Bundle args = new Bundle();
        args.putInt(FRAGMENT_STATUS_ARG, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIssuesAdapter = new IssuesAdapter(getContext(), new ArrayList<Issue>(0), mItemListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_with_recycler_view, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        mActionsListener = new IssuesPresenter(new IssueRepository(), this);
        mActionsListener.loadIssues();
        Bundle args = getArguments();
        mState = args.getInt(FRAGMENT_STATUS_ARG);
        mActionsListener.loadIssuesByState(mState);
    }

    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.issues_list_recyclerview);
        recyclerView.setAdapter(mIssuesAdapter);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener =
            new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int previousTotal = 0;
                    int visibleTreshold = 4;
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = mLinearLayoutManager.getItemCount();
                    int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
                    if (totalItemCount > previousTotal) {
                        mLoading = false;
                        previousTotal = totalItemCount;
                    }
                    if (!mLoading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleTreshold)) {
                        Log.d(LOG_TAG, "Last item");
                        mActionsListener.loadIssues();
                        mActionsListener.loadIssuesByState(mState);
                        mLoading = true;
                    }
                }
            };

    IssueItemListener mItemListener = new IssueItemListener() {
        @Override
        public void onIssueClick(Issue clickedIssue) {
            mActionsListener.openIssueDetails(clickedIssue);
        }
    };

    @Override
    public void showIssues(List<Issue> issues) {
        {
            mIssuesAdapter.replaceData(issues);
        }
    }

    @Override
    public void showIssueDetailUi(int issueId) {
        Intent intent = new Intent(getContext(), IssueDetailActivity.class);
        intent.putExtra(IssueDetailActivity.EXTRA_ISSUE_ID, issueId);
        startActivity(intent);
    }

    public interface IssueItemListener {

        void onIssueClick(Issue clickedIssue);
    }
}
