package com.example.lullichka.yalantis_2.issuedetail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lullichka.yalantis_2.R;
import com.example.lullichka.yalantis_2.adapters.PicturesAdapter;
import com.example.lullichka.yalantis_2.data.IssueRepository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Activity for showing details of chosen Issue
 */

public class IssueDetailActivity extends AppCompatActivity implements IssueDetailContract.View {

    public static final String EXTRA_ISSUE_ID = "ISSUE_ID";
    private static final String LOG_TAG = "mylog";

    @BindView(R.id.description)
    TextView mDescription;
    @BindView(R.id.tv_status)
    TextView mStatus;
    @BindView(R.id.date_of_creating)
    TextView mDateCreate;
    @BindView(R.id.date_of_registration)
    TextView mDateRegister;
    @BindView(R.id.date_of_solution)
    TextView mDateSolve;
    @BindView(R.id.tv_header)
    TextView mDepartment;
    @BindView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.toolbar_actionbar)
    Toolbar toolbar;

    @OnClick({R.id.description,
            R.id.tv_status,
            R.id.date_of_creating,
            R.id.date_of_registration,
            R.id.date_of_solution,
            R.id.tv_header,
            R.id.my_recycler_view})
    void onClick(View view) {
        Toast.makeText(this, view.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IssueDetailContract.UserActionsListener  actionsListener = new IssueDetailPresenter(new IssueRepository(), this);
        setContentView(R.layout.issues_detail_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        }
        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
        }
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);

        //Retrieving data from intent
        Integer issueId = getIntent().getIntExtra(EXTRA_ISSUE_ID, 0);
        Log.d(LOG_TAG, issueId.toString());
        actionsListener.openIssue(issueId);

        getSupportActionBar().setTitle(String.valueOf(issueId));

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }


    @Override
    public void showCreatedDate(String createdDate) {
        mDateCreate.setText(createdDate);
    }

    @Override
    public void showRegisterDate(String registerDate) {
        mDateRegister.setText(registerDate);
    }

    @Override
    public void showDeadline(String deadline) {
        mDateSolve.setText(deadline);
    }

    @Override
    public void showDepartment(String department) {
        mDepartment.setText(department);
    }

    @Override
    public void showDescription(String description) {
        mDescription.setText(description);
    }

    @Override
    public void showState(String state) {
        mStatus.setText(state);
    }

    @Override
    public void showPictures(List<String> picturesUrls) {
        RecyclerView.Adapter adapter = new PicturesAdapter(getApplicationContext(), picturesUrls);
        mRecyclerView.setAdapter(adapter);
    }
}

