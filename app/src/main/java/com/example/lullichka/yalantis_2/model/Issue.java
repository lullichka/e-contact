package com.example.lullichka.yalantis_2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Model class for Issue description
 */

public class Issue extends RealmObject  {


    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("geo_address")
    @Expose
    private GeoAddress geoAddress;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("created_date")
    @Expose
    private Integer createdDate;
    @SerializedName("start_date")
    @Expose
    private Integer startDate;
    @SerializedName("state")
    @Expose
    private State state;
    @SerializedName("ticket_id")
    @Expose
    private String ticketId;
    @SerializedName("files")
    @Expose
    private RealmList<File> files = new RealmList<>();
    @SerializedName("performers")
    @Expose
    private RealmList<Performer> performers = new RealmList<>();
    @SerializedName("deadline")
    @Expose
    private Integer deadline;
    @SerializedName("likes_counter")
    @Expose
    private Integer likesCounter;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The geoAddress
     */
    public GeoAddress getGeoAddress() {
        return geoAddress;
    }

    /**
     * @param geoAddress The geo_address
     */
    public void setGeoAddress(GeoAddress geoAddress) {
        this.geoAddress = geoAddress;
    }

    /**
     * @return The category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return The type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The createdDate
     */
    public Integer getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate The created_date
     */
    public void setCreatedDate(Integer createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return The startDate
     */
    public Integer getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The start_date
     */
    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return The ticketId
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * @param ticketId The ticket_id
     */
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     * @return The files
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * @param files The files
     */
    public void setFiles(RealmList<File> files) {
        this.files = files;
    }

    /**
     * @return The performers
     */
    public List<Performer> getPerformers() {
        return performers;
    }

    /**
     * @param performers The performers
     */
    public void setPerformers(RealmList<Performer> performers) {
        this.performers = performers;
    }

    /**
     * @return The deadline
     */
    public Integer getDeadline() {
        return deadline;
    }

    /**
     * @param deadline The deadline
     */
    public void setDeadline(Integer deadline) {
        this.deadline = deadline;
    }

    /**
     * @return The likesCounter
     */
    public Integer getLikesCounter() {
        return likesCounter;
    }

    /**
     * @param likesCounter The likes_counter
     */
    public void setLikesCounter(Integer likesCounter) {
        this.likesCounter = likesCounter;
    }

}