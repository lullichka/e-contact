package com.example.lullichka.yalantis_2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lullichka.yalantis_2.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Adapter for pictures in IssueDetailActivity
 */

public class PicturesAdapter extends RecyclerView.Adapter<PicturesAdapter.ViewHolder> {
    private final List<String> mPictures;
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;

    public PicturesAdapter(Context context, List<String>pictures) {
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
        mPictures = pictures;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.my_image_view)
        ImageView imageView;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    @OnClick(R.id.my_image_view)
    void onClick(View view) {
        Toast.makeText(mContext, view.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.pict_adapter_image_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(mContext)
                .load(mPictures.get(position))
                .fit()
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }

}
