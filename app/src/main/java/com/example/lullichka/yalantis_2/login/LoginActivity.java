package com.example.lullichka.yalantis_2.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lullichka.yalantis_2.R;
import com.example.lullichka.yalantis_2.data.AccessTokenRepo;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lullichka on 11.05.16.
 */
public class LoginActivity extends AppCompatActivity implements LoginActivityContract.View {

    @Nullable
    @BindView(R.id.toolbar_actionbar)
    Toolbar toolbar;
    @Nullable
    @BindView(R.id.user_image)
    ImageView mUserImageView;
    @Nullable
    @BindView(R.id.user_name)
    TextView mUserTextView;
    private static final String LOG_TAG = "mylog";
    private CallbackManager mCallbackManager;
    private LoginActivityPresenter mActionsListener;
    private AccessTokenTracker mAccessTokenTracker;
    private AccessToken mAccessToken;
    private ProfileTracker mProfileTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        mActionsListener = new LoginActivityPresenter(new AccessTokenRepo(), this);
        ButterKnife.bind(this);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_activity);
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        List<String> permissionNeeds = Arrays.asList(getResources().getStringArray(R.array.facebook_permissions));
        if (loginButton != null) {
            loginButton.setReadPermissions(permissionNeeds);
            loginButton.registerCallback(mCallbackManager, mCallback);
        }
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (Profile.getCurrentProfile() != null && currentAccessToken != null) {
                    mActionsListener.saveToken(Profile.getCurrentProfile(), currentAccessToken);
                }
            }
        };
        mAccessToken = AccessToken.getCurrentAccessToken();
        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (currentProfile == null) {
                    if (mUserImageView != null && mUserTextView != null) {
                        mUserImageView.setVisibility(View.INVISIBLE);
                        mUserTextView.setVisibility(View.INVISIBLE);
                    }
                } else {
                    showUserProfile(currentProfile);
                }
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            showUserProfile(Profile.getCurrentProfile());
        }

        @Override
        public void onCancel() {
            Log.d(LOG_TAG, "Action cancelled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d(LOG_TAG, "Error accesing Facebook");
        }
    };

    @Override
    public void showUserProfile(Profile profile) {
        if (profile != null) {
            Log.d(LOG_TAG, "showUserProfile " + profile.getName());
            ButterKnife.bind(this);
            if (mUserImageView != null) {
                mUserImageView.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext())
                        .load(profile.getProfilePictureUri(180, 180))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .into(mUserImageView);
            }
            if (mUserTextView != null) {
                mUserTextView.setVisibility(View.VISIBLE);
                mUserTextView.setText(getResources().getString(R.string.hello_user, profile.getName()));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAccessTokenTracker.stopTracking();
        mProfileTracker.stopTracking();
    }
}
