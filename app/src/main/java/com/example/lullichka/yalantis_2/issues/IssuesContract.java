package com.example.lullichka.yalantis_2.issues;

import android.support.annotation.NonNull;

import com.example.lullichka.yalantis_2.model.Issue;

import java.util.List;

/**
 * Created by lullichka on 18.05.16.
 */
public interface IssuesContract {

    interface View {

    void showIssues(List<Issue> issues);

    void showIssueDetailUi(int issueId);

    }

    interface UserActionsListener {

        void loadIssues();

        void loadIssuesByState(int state);

        void openIssueDetails(@NonNull Issue requestedIssue);
    }
}
