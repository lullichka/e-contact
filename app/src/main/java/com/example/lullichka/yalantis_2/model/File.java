package com.example.lullichka.yalantis_2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lullichka on 13.05.16.
 */
public class File extends RealmObject {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("filename")
        @Expose
        private String filename;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         * The filename
         */
        public String getFilename() {
            return filename;
        }

        /**
         *
         * @param filename
         * The filename
         */
        public void setFilename(String filename) {
            this.filename = filename;
        }


}
