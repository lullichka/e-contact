package com.example.lullichka.yalantis_2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lullichka.yalantis_2.R;
import com.example.lullichka.yalantis_2.issues.IssueFragment;
import com.example.lullichka.yalantis_2.model.Issue;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lullichka on 24.05.16.
 * Adapter for Issues in IssuesFragment
 */

public class IssuesAdapter extends RecyclerView.Adapter<IssuesAdapter.IssuesRecyclerViewHolder> {
    private static final String STRING_URL = "http://dev-contact.yalantis.com/files/ticket/";
    private IssueFragment.IssueItemListener mItemListener;
    private Context mContext;
    private List<Issue> mIssuesList;

    public IssuesAdapter(Context context, List<Issue> issues, IssueFragment.IssueItemListener itemListener) {
        setList(issues);
        mItemListener = itemListener;
        mIssuesList = issues;
        mContext = context;
    }

    public void replaceData(List<Issue> issues) {
        setList(issues);
        notifyDataSetChanged();
    }

    private void setList(List<Issue> issues) {
        mIssuesList = issues;
    }

    @Override
    public IssuesRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issueView = inflater.inflate(R.layout.recycler_view_list_item, parent, false);
        return new IssuesRecyclerViewHolder(issueView, mItemListener);
    }

    @Override
    public void onBindViewHolder(IssuesRecyclerViewHolder holder, int position) {
        Issue issue = mIssuesList.get(position);
        holder.description.setText(issue.getTitle());
        if (issue.getUser().getAddress().getStreet() != null) {
            String address = issue.getUser().getAddress().getStreet().getStreetType().getShortName() + " " +
                    issue.getUser().getAddress().getStreet().getName() + ", " +
                    issue.getUser().getAddress().getHouse().getName();
            holder.address.setText(address);
        }
        Date date = new Date(issue.getCreatedDate());
        SimpleDateFormat ft = new SimpleDateFormat("MMM DD, yyyy");
        String dateToStr = ft.format(date);
        holder.dateCreate.setText(dateToStr);
        holder.likes.setText(String.valueOf(issue.getLikesCounter()));
        String imageCategory = STRING_URL + issue.getCategory().getImage();
        Picasso.with(mContext)
                .load(imageCategory)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.business_sheet)
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return mIssuesList == null ? 0 : mIssuesList.size();

    }

    class IssuesRecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.issue_description)
        TextView description;
        @BindView(R.id.issue_address)
        TextView address;
        @BindView(R.id.date_of_registration)
        TextView dateCreate;
        @BindView(R.id.likes_recyclerview)
        TextView likes;
        @BindView(R.id.department_icon)
        ImageView icon;

        private IssuesRecyclerViewHolder(View itemView, IssueFragment.IssueItemListener listener) {
            super(itemView);
            mItemListener = listener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Issue issue = getItem(position);
                    mItemListener.onIssueClick(issue);
                }
            });
        }

        private Issue getItem(int position) {
            return mIssuesList.get(position);
        }
    }
}


