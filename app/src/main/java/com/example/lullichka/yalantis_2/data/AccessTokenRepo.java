package com.example.lullichka.yalantis_2.data;

import android.util.Log;

import com.example.lullichka.yalantis_2.localdata.LocalUserInfo;
import com.facebook.AccessToken;
import com.facebook.Profile;

import java.util.UUID;

import io.realm.Realm;

/**
 * Created by lullichka on 28.05.16.
 * Repository for AccessToken saving
 */

public class AccessTokenRepo implements InterfaceAccessTokenRepo {

    private static final String LOG_TAG = "mylog";

    @Override
    public void saveToken(Profile profile, AccessToken accessToken, SaveTokenCallback callback) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        // Create an object
        LocalUserInfo localUser = realm.createObject(LocalUserInfo.class);
        // Set its fields
        String localUserId = String.valueOf(UUID.randomUUID());
        localUser.setId(localUserId);
        localUser.setFirstName(profile.getFirstName());
        localUser.setLastName(profile.getLastName());
        localUser.setAccessToken(String.valueOf(accessToken));
        localUser.setProfilePhoto(String.valueOf(profile.getProfilePictureUri(150, 150)));
        Log.d(LOG_TAG, "accesstoken saved");
        realm.commitTransaction();
        realm.close();
        callback.onTokenSaved(localUserId);
    }

}
