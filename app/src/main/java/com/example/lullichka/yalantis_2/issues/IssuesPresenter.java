package com.example.lullichka.yalantis_2.issues;

import android.util.Log;

import com.example.lullichka.yalantis_2.data.InterfaceIssueRepository;
import com.example.lullichka.yalantis_2.model.Issue;

import java.util.List;

/**
 * Created by lullichka on 18.05.16.
 * Presenter for Issue Fragment
 */
public class IssuesPresenter implements IssuesContract.UserActionsListener {

    private static final String LOG_TAG = "myliog";
    private final InterfaceIssueRepository mInterfaceIssueRepository;
    private final IssuesContract.View mIssuesView;

    public IssuesPresenter( InterfaceIssueRepository interfaceIssueRepository,
                            IssuesContract.View issuesView) {
        mInterfaceIssueRepository = interfaceIssueRepository;
        mIssuesView = issuesView;
    }

    @Override
    public void loadIssues() {
        mInterfaceIssueRepository.getIssues(new InterfaceIssueRepository.LoadIssuesCallback() {
            @Override
            public void onIssuesLoaded(List<Issue> issues) {
                Log.d(LOG_TAG, "Server loader");
                return;
            }
        });
    }

    @Override
    public void loadIssuesByState(int state) {
        mInterfaceIssueRepository.getIssuesByState(state, new InterfaceIssueRepository.LoadIssuesCallback() {
            @Override
            public void onIssuesLoaded(List<Issue> issues) {
                mIssuesView.showIssues(issues);
            }
        });
    }
    @Override
    public void openIssueDetails(Issue requestedIssue) {
        mIssuesView.showIssueDetailUi(requestedIssue.getId());
    }
}
