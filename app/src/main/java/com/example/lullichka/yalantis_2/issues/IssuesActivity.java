package com.example.lullichka.yalantis_2.issues;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.lullichka.yalantis_2.login.LoginActivity;
import com.example.lullichka.yalantis_2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Activity where app starts at the moment. Shows View Pager with three Tabs, each for Fragment -
 * Issues Done, Not done and In process. Activity also has a navigation drawer and floating action button
 * which hides with animation when user scrolls.
 */

public class IssuesActivity extends AppCompatActivity {

    private final int STATUS_IN_PROGRESS = 1;
    private final int STATUS_PENDING = 2;
    private final int STATUS_DONE = 3;

    @BindView(R.id.toolbar_actionbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.issues_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_menu);
        }
        getSupportActionBar().setTitle(R.string.all_requests);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.sign_in:
                            startActivity(new Intent(IssuesActivity.this, LoginActivity.class));
                            break;
                        default:
                            break;
                    }
                    // Close the navigation drawer when an item is selected.
                    item.setChecked(true);
                    if (drawer != null) {
                        drawer.closeDrawers();
                    }
                    return true;
                }
            });
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tablayout = (TabLayout) findViewById(R.id.tabs);
        if (tablayout != null) {
            tablayout.setupWithViewPager(viewPager);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

    }

    private void setupViewPager(final ViewPager viewPager) {

        //Setup adapter for viewpager
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(IssueFragment.newInstance(STATUS_IN_PROGRESS), getResources().getString(R.string.at_work));
        adapter.addFragment(IssueFragment.newInstance(STATUS_DONE), getResources().getString(R.string.done));
        adapter.addFragment(IssueFragment.newInstance(STATUS_PENDING), getResources().getString(R.string.waiting));
        viewPager.setAdapter(adapter);

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList();
        private final List<String> mFragmentTitleList = new ArrayList();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
