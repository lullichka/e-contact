package com.example.lullichka.yalantis_2.localdata;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lullichka on 26.05.16.
 */
public class LocalUserInfo extends RealmObject {
    @PrimaryKey
    private String id;
    private String firstName;
    private String lastName;
    private String profilePhoto;
    private String token;

    public LocalUserInfo() {

    }

    public LocalUserInfo(String id, String firstName, String lastName, String profilePhoto) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilePhoto = profilePhoto;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAccessToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
