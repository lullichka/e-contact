package com.example.lullichka.yalantis_2.login;

import com.facebook.AccessToken;
import com.facebook.Profile;

/**
 * Created by lullichka on 28.05.16.
 */
public interface LoginActivityContract {

    interface UserActionsListener{

        void saveToken(Profile profile, AccessToken accessToken);

    }

    interface View{

        void showUserProfile(Profile profile);
    }
}
