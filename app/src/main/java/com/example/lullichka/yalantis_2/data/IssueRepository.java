package com.example.lullichka.yalantis_2.data;

import android.util.Log;

import com.example.lullichka.yalantis_2.model.Issue;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lullichka on 20.05.16.
 * Class implements loading data from server and all work with local database
 */

public class IssueRepository implements InterfaceIssueRepository {

    private static final int AMOUNT_OF_ISSUES = 20;
    private static int OFFSET = 0;
    private final int STATUS_IN_PROGRESS = 1;
    private final int STATUS_PENDING = 2;
    private final int STATUS_DONE = 3;
    private Realm mRealm;

    private static final String BASE_URL = "http://dev-contact.yalantis.com/rest/v1/";
    private static final String LOG_TAG = "mylog";

    @Override
    public void getIssues(final LoadIssuesCallback callback) {
        mRealm = Realm.getDefaultInstance();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<List<Issue>> call = service.getIssues(OFFSET, AMOUNT_OF_ISSUES);
        call.enqueue(new Callback<List<Issue>>() {
            @Override
            public void onResponse(Call<List<Issue>> call, Response<List<Issue>> response) {
                List<Issue> issueList = response.body();
                Log.d(LOG_TAG, "server answer in quantity " + issueList.size());
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(issueList);
                mRealm.commitTransaction();
                if (issueList.size() > 0) {
                    OFFSET = OFFSET + AMOUNT_OF_ISSUES;
                }
                Log.d(LOG_TAG, "offset now is " + OFFSET);
                callback.onIssuesLoaded(issueList);
            }

            @Override
            public void onFailure(Call<List<Issue>> call, Throwable t) {
                Log.d(LOG_TAG, "something happened with server");
            }
        });
    }

    @Override
    public void getIssuesByState(int state, LoadIssuesCallback callback) {
        mRealm = Realm.getDefaultInstance();
        List<Issue> responseIssueList = new ArrayList<>(0);
        switch (state) {
            case STATUS_IN_PROGRESS:
                RealmQuery<Issue> queryInProgress = mRealm.where(Issue.class);
                queryInProgress.equalTo("state.id", 0);
                queryInProgress.or().equalTo("state.id", 5);
                queryInProgress.or().equalTo("state.id", 7);
                queryInProgress.or().equalTo("state.id", 8);
                queryInProgress.or().equalTo("state.id", 9);
                responseIssueList = queryInProgress.findAll();
                Log.d(LOG_TAG, "in progress data queried, " + responseIssueList.size());
                break;
            case STATUS_DONE:
                RealmQuery<Issue> queryDone = mRealm.where(Issue.class);
                queryDone.equalTo("state.id", 6);
                queryDone.or().equalTo("state.id", 10);
                responseIssueList = queryDone.findAll();
                Log.d(LOG_TAG, "done data queried, " + responseIssueList.size());
                break;
            case STATUS_PENDING:
                RealmQuery<Issue> queryPending = mRealm.where(Issue.class);
                queryPending.equalTo("state.id", 1);
                queryPending.or().equalTo("state.id", 3);
                queryPending.or().equalTo("state.id", 4);
                responseIssueList = queryPending.findAll();
                Log.d(LOG_TAG, "pending data queried, " + responseIssueList.size());
                break;
        }
        mRealm.close();
        callback.onIssuesLoaded(responseIssueList);
    }

    @Override
    public void getIssue(final int issueId, final GetIssueCallback callback) {
        mRealm = Realm.getDefaultInstance();
        RealmQuery<Issue> query = mRealm.where(Issue.class);
        query.equalTo("id", issueId);
        Issue issue = query.findFirst();
        mRealm.close();
        callback.onIssueLoaded(issue);
    }
}
