package com.example.lullichka.yalantis_2.data;

import com.example.lullichka.yalantis_2.model.Issue;

import java.util.List;

/**
 * Main entry point for accessing issues data.
 */
public interface InterfaceIssueRepository {

    interface LoadIssuesCallback {

        void onIssuesLoaded(List<Issue> issues);
    }


    interface GetIssueCallback {

        void onIssueLoaded(Issue issue);
    }

    void getIssues(LoadIssuesCallback callback);

    void getIssuesByState(int state, LoadIssuesCallback callback);

    void getIssue(int issueId,  GetIssueCallback callback);

}

