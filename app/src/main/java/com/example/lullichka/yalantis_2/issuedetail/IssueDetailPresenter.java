package com.example.lullichka.yalantis_2.issuedetail;

import android.support.annotation.Nullable;
import android.util.Log;

import com.example.lullichka.yalantis_2.data.InterfaceIssueRepository;
import com.example.lullichka.yalantis_2.model.File;
import com.example.lullichka.yalantis_2.model.Issue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lullichka on 18.05.16.
 * Presenter for IssueDetailActivity
 */

public class IssueDetailPresenter implements IssueDetailContract.UserActionsListener {
    private static final String STRING_URL = "http://dev-contact.yalantis.com/files/ticket/";
    private static final String LOG_TAG = "mylog";
    private final IssueDetailContract.View mIssueDetailView;
    private InterfaceIssueRepository mIssueRepository;


    public IssueDetailPresenter(InterfaceIssueRepository interfaceIssueRepository,
                                IssueDetailContract.View issueDetailView) {
        mIssueRepository = interfaceIssueRepository;
        mIssueDetailView = issueDetailView;
    }

    @Override
    public void openIssue(@Nullable final int issueId) {
        if (issueId == 0) {
            Log.d(LOG_TAG, "Issue id missing");
            return;
        }
        mIssueRepository.getIssue(issueId, new InterfaceIssueRepository.GetIssueCallback() {
            @Override
            public void onIssueLoaded(Issue issue) {
                showIssue(issue);
            }
        });
    }

    private void showIssue(Issue issue) {
        Date date = new Date(issue.getCreatedDate());
        Date deadline = new Date(issue.getDeadline());
        SimpleDateFormat ft = new SimpleDateFormat("MMM DD, yyyy");
        String createdDate = ft.format(date);
        String deadlineDate = ft.format(deadline);
        String description = issue.getBody();
        String status = issue.getState().getName();
        String department = issue.getCategory().getName();
        List<File> filenames = issue.getFiles();
        List<String> pictures = new ArrayList<>();
        for (File file : filenames) {
            pictures.add(STRING_URL + file.getFilename());
        }

        if (description != null) {
            mIssueDetailView.showDescription(description);
        }
        mIssueDetailView.showCreatedDate(createdDate);
        mIssueDetailView.showRegisterDate(createdDate);
        mIssueDetailView.showDeadline(deadlineDate);
        if (status != null) {
            mIssueDetailView.showState(status);
        }
        if (department != null) {
            mIssueDetailView.showDepartment(department);
        }
        mIssueDetailView.showPictures(pictures);
    }
}
